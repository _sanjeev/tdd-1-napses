const chai = require('chai');
const expect = chai.expect;

describe('Find last but one element in a list', () => {
    context('when no list is passing', () => {
        it('should return nothing', () => {
            const result = findLastButOneInList();
            expect(result).to.be.undefined;
        })
    })
    context('when the list is empty', () => {
        it('should return nothing', () => {
            const result = findLastButOneInList([]);
            expect(result).to.be.undefined;
        })
    })
    context('when the list contains one element', () => {
        it('should return nothing', () => {
            const result = findLastButOneInList([1]);
            expect(result).to.be.undefined;
        })
    })
    context ('when the list contains two element', () => {
        it('should return first element', () => {
            const result = findLastButOneInList([1, 2]);
            expect(result).to.be.eql(1);
        })
    })
    context('when list contains more than two element', () => {
        it('should return the second last element', () => {
            const result = findLastButOneInList([1, 2, 3, 4]);
            expect(result).to.be.eql(3);
        })
    })
})

const findLastButOneInList = (list = []) => {
    return list[list.length - 2];
}