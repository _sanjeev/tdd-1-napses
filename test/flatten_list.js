const chai = require('chai');
const expect = chai.expect;

describe ('Flatten a nexted list', () => {
    context('When no list is passed', () => {
        it ('Should return nothing', () => {
            const result = flattenList ();
            expect(result).to.be.undefined;
        })
    })
    context('When the list is empty', () => {
        it ('Should return nothing', () => {
            const result = flattenList ([]);
            expect(result).to.be.undefined;
        })
    })
    context('When the list contains one subarray', () => {
        it ('Should return list which contains only element', () => {
            const result = flattenList ([[1]]);
            expect(result).to.be.eql([1]);
        })
    })
    context('When the list contains more than one subarray', () => {
        it ('Should return list which contains only element', () => {
            const result = flattenList ([[1], [2, 3], [3, 4, 5]]);
            expect(result).to.be.eql([1, 2, 3, 3, 4, 5]);
        })
    })
})

const flattenList = (list = []) => {
    return list.length === 0 ? undefined : list.flat();
}