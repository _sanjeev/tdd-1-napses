const chai = require('chai');
const expect = chai.expect;

describe('Pack consecutive dublicates of list elements into sublist', () => {
    context('When the list is not passed', () => {
        it ('Should return nothing', () => {
            const result = peakConsecutiveDublicates();
            expect(result).to.be.undefined;
        })
    })
    context('When the list is empty', () => {
        it ('Should return nothing', () => {
            const result = peakConsecutiveDublicates([]);
            expect(result).to.be.undefined;
        })
    })
    context('When the list contains one element', () => {
        it ('Should return that element', () => {
            const result = peakConsecutiveDublicates(['a']);
            expect(result).to.be.eql(['a']);
        })
    })
    context('When the list contains two element', () => {
        it ('Should return that element', () => {
            const result = peakConsecutiveDublicates(['a', 'b']);
            expect(result).to.be.eql(['a', 'b']);
        })
    })
    context ('When the list contains more than two element', () => {
        it ('Should return the pack consecutives dublicates', () => {
            const result = peakConsecutiveDublicates (['a', 'a', 'a', 'a', 'b', 'c', 'c', 'a', 'a', 'd', 'e', 'e', 'e', 'e']);
            expect(result).to.be.eql(["aaaa","b","cc","aa","d","eeee"]);
        })
    })
})

const peakConsecutiveDublicates = (list = []) => {
    if (list.length === 0) {
        return undefined;
    }
    let starting_index = 1, finarResult = [], result = list[0], index = 0;
    while(starting_index < list.length) {
        if (list[starting_index] === list[starting_index - 1]) {
            result += list[starting_index];
            starting_index++;
        }else {
            finarResult.push(result);
            result = '';
            result += list[starting_index];
            starting_index++;
        }
    }
    finarResult.push (result);
    return finarResult;
}