const chai = require('chai');
const expect = chai.expect;

describe('Check Wheather a list is palindrome or not', () => {
    context('When no list is passed', () => {
        it('Should return nothing', () => {
            const result = checkPalindrome();
            expect(result).to.be.undefined;
        })
    })
    context('When the list is empty', () => {
        it('Should return nothing', () => {
            const result = checkPalindrome([]);
            expect(result).to.be.undefined;
        })
    })
    context('When the list contains one element', () => {
        it('Should return true', () => {
            const result = checkPalindrome([1]);
            expect(result).to.be.eql(true);
        })
    })
    context('When the list contains two element', () => {
        it('Should return false', () => {
            const result = checkPalindrome([1, 2]);
            expect(result).to.be.eql(false);
        })
    })
    context('When the list contains more than two element', () => {
        it('Should return false', () => {
            const result = checkPalindrome([1, 2, 3]);
            expect(result).to.be.eql(false);
        })
    })
    context('When the list contains more than two element', () => {
        it('Should return false', () => {
            const result = checkPalindrome([1, 2, 1]);
            expect(result).to.be.eql(true);
        })
    })
})

const checkPalindrome = (list = []) => {
    if (list.length === 0) {
        return undefined;
    }
    let starting_index = 0, ending_index = list.length - 1;
    while(starting_index !== ending_index) {
        if (list[starting_index] !== list[ending_index]) {
            return false;
        }
        starting_index += 1;
        ending_index -= 1;
    }
    return true;
}