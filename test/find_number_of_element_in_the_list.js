const chai = require('chai');
const expect = chai.expect;

describe('Find the number of element in the list', () => {
    context('When the list is empty', () => {
        it('should return 0', () => {
            const result = findLengthOfTheList([]);
            expect(result).to.be.eql(0);
        })
    })
    context('When the list ontains one element', () => {
        it('should return 1', () => {
            const result = findLengthOfTheList([1]);
            expect(result).to.be.eql(1);
        })
    })
    context ('When the list contains more than one element', () => {
        it('Should return no of element in the list', () => {
            const result = findLengthOfTheList([1, 2, 3]);
            expect(result).to.be.eql(3);
        })
    })
})
const findLengthOfTheList = (list = []) => {
    return list.length;
}