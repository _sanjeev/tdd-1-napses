const chai = require('chai');
const expect = chai.expect;

describe('Reverse of a List', () => {
    context ('When the list is not passed', () => {
        it('Should return nothing', () => {
            const result = reverseOfList();
            expect(result).to.be.undefined;
        })
    })
    context('When the list is empty', () => {
        it('Should return nothing', () => {
            const result = reverseOfList([]);
            expect(result).to.be.undefined;
        })
    })
    context ('When the list contains one element', () => {
        it ('should return the same list', () => {
            const result = reverseOfList([1]);
            expect(result).to.be.eql([1]);
        })
    })
    context ('When the list contains more than one element', () => {
        it ('should return the reversed list', () => {
            const result = reverseOfList([1, 2, 3]);
            expect(result).to.be.eql([3, 2, 1]);
        })
    })
})

const reverseOfList = (list = []) => {
    return list.length === 0 ? undefined : list.reverse();
}