const chai = require ('chai');
const expect = chai.expect;

describe ('Find Kth element in the list', () => {
    context ('When the list is empty and k is 1', () => {
        it ('should return nothing', () => {
            const result = findKthElement ([], 1);
            expect(result).to.be.undefined;
        })
    })
    context ('When the list contains one element and k is 0', () => {
        it ('should return nothing', () => {
            const result = findKthElement ([1], 0);
            expect(result).to.be.undefined;
        })
    })
    context ('When the list contains one element and k is greater than the length of the list', () => {
        it ('should return nothing', () => {
            const result = findKthElement ([1], 2);
            expect(result).to.be.undefined;
        })
    })
    context ('When the list contains one element and k is 1', () => {
        it ('should return first element', () => {
            const result = findKthElement ([1], 1);
            expect(result).to.be.eql(1);
        })
    })
    context ('When the list contains two element and k is 2', () => {
        it ('should return second element', () => {
            const result = findKthElement ([1, 3], 2);
            expect(result).to.be.eql(3);
        })
    })
    context ('When the list contains more than two elements and k is 3', () => {
        it ('should return third element', () => {
            const result = findKthElement ([1, 3, 4, 5, 6], 3);
            expect(result).to.be.eql(4);
        })
    })
})

const findKthElement = (list , k) => {
    if (list.length === 0 || k < 1 || k > list.length) {
        return undefined;
    }
    return list[k - 1];
}