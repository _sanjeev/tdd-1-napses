const chai = require("chai");
const expect = chai.expect;

describe("Eliminate Consecutive Dublicates of list", () => {
  context("When no list is passed", () => {
    it("Should return nothing", () => {
      const result = eliminateDublicates();
      expect(result).to.be.undefined;
    });
  });
  context("When the list is empty", () => {
    it("Should return nothing", () => {
      const result = eliminateDublicates([]);
      expect(result).to.be.undefined;
    });
  });
  context("When the list contains one element", () => {
    it("Should return list which contains first element", () => {
      const result = eliminateDublicates(["a"]);
      expect(result).to.be.eql(["a"]);
    });
  });
  context("When the list contains more than one element", () => {
    it("Should return updated consecutive list", () => {
      const result = eliminateDublicates(["a", "a", "a", "b", "b"]);
      expect(result).to.be.eql(["a", "b"]);
    });
  });
  context("When the list contains more than one element", () => {
    it("Should return updated consecutive string", () => {
      const result = eliminateDublicates("aaaabccaadeeee");
      expect(result).to.be.eql("abcade");
    });
  });
});

const eliminateDublicates = (list = []) => {
  if (list.length === 0) {
    return undefined;
  }
  let starting_index = 0,
    ending_index = 1,
    finalResult = "";
  while (ending_index < list.length) {
    if (list[starting_index] !== list[ending_index]) {
      finalResult += list[starting_index];
      starting_index = ending_index;
    }
    ending_index += 1;
  }
  finalResult += list[list.length - 1];
  return typeof list !== "string" ? finalResult.split("") : finalResult;
};
