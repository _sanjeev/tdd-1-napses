const chai = require('chai');
const expect = chai.expect;

describe('Find the last element of a list', () => {
    context('when no list is passed', () => {
        it('should return nothing', () => {
            const result = findLastElementInList();
            expect(result).to.be.undefined;
        })
    })
    context('when the list is empty', () => {
        it('should return nothing', () => {
            const result = findLastElementInList([]);
            expect(result).to.be.undefined;
        })
    })
    context('when the list contains one element', () => {
        it('should return first element', () => {
            const result = findLastElementInList([1]);
            expect(result).to.be.eql(1);
        })
    })
    context('when the list contains more than one element', () => {
        it('should return last element in the list', () => {
            const result = findLastElementInList([1, 2, 3]);
            expect(result).to.be.eql(3);
        })
    })
})

const findLastElementInList = (list = []) => {
    return list[list.length - 1];
} 